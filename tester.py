import torch
import cv2
import numpy as np
vid = cv2.VideoCapture(0)
model = torch.hub.load('ultralytics/yolov5', 'yolov5s') 
  
while(True):
      
    # Capture the video frame
    # by frame
    ret, frame = vid.read()
    try:
        results=model(frame)
        # frame=np.array(frame)
    except:
        frame=frame
    
        # Display the resulting frame
    cv2.imshow('frame', frame)
    
    # the 'q' button is set as the
    # quitting button you may use any
    # desired button of your choice
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
  
# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()